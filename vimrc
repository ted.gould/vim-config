" This is Ted's vimrc

set nocompatible

" Check for system config
if filereadable("/usr/share/vim/vim80/debian.vim")
    source /usr/share/vim/vim80/debian.vim
endif

" Color themes
syntax on
if has('gui_running')
  set background=light
else
  set background=dark
endif

" Ignore compiled files
set wildignore=*.o,*~,*.pyc
set wildignore+=.git\*,.hg\*,.svn\*
set wildignore+=node_modules\*",dash/static/*
set wildignore+=vendor\*
set wildignore+=pkg\*
set wildignore+=test-profiles\*
set wildignore+=testbin\*

" Settings
set ruler
set rulerformat=%l,%c%V%=%P
set hid
set backspace=eol,start,indent
set whichwrap+=<,>h,l
set lazyredraw
set magic
set showmatch
set mat=2
set foldcolumn=0
set encoding=utf8
set ffs=unix,dos,mac
set laststatus=2
set completeopt-=preview
set history=1000
set undofile
set undoreload=1000
set shell=/bin/bash
set scrolloff=10
set mouse=a
set nu
set nowrap

" Tabs
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set ai "Auto indent
set si "Smart indent

" Search
set ignorecase
set smartcase
set hlsearch
set incsearch

" Directories
set backupskip=/tmp/*,/private/tmp/*

silent !mkdir -p ~/.cache/tmp/undo > /dev/null 2>&1
set undodir=~/.cache/tmp/undo//     " undo files

silent !mkdir -p ~/.cache/tmp/backup > /dev/null 2>&1
set backupdir=~/.cache/tmp/backup// " backups

silent !mkdir -p ~/.cache/tmp/swap > /dev/null 2>&1
set directory=~/.cache/tmp/swap//   " swap files

set backup                          " enable backups
set noswapfile                      " It's 2012, Vim.
set noautochdir

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_loc_list_height = 5
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1

let g:syntastic_error_symbol = '❌'
let g:syntastic_style_error_symbol = '⁉️'
let g:syntastic_warning_symbol = '⚠️'
let g:syntastic_style_warning_symbol = '💩'

highlight link SyntasticErrorSign SignColumn
highlight link SyntasticWarningSign SignColumn
highlight link SyntasticStyleErrorSign SignColumn
highlight link SyntasticStyleWarningSign SignColumn

" Watchly Go Syntastic Metalinter config
if filereadable(expand('$HOME/source/watchly/gometalinter.sh'))
    let g:syntastic_go_checkers = ['gometalinter']
    let g:syntastic_go_gometalinter_exe = expand('$HOME/source/watchly/gometalinter.sh')
endif

" vim md
let g:vim_markdown_folding_disabled = 1

" Yaml
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType yml setlocal ts=2 sts=2 sw=2 expandtab
